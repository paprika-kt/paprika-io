package paprika.io.buffer

import java.nio.ByteBuffer
import java.nio.ByteOrder

actual class ShortArrayBuffer actual constructor(override val size: Int)
    : TypedArrayBuffer<Short> {

    override var capacity: Int = size * 2
    override var byteLength: Int = 0
    override var position: Int
        get() = buffer.position()
        set(value) {
            buffer.position(value)
        }

    override fun reset() {
        buffer.position(0)
        byteLength = 0
    }

    override val buffer = ByteBuffer.allocateDirect(size * 4)
        .order(ByteOrder.nativeOrder())
        .asShortBuffer()

    override fun set(index: Int, value: Short) {
        val length = (index + 1) * 2
        if (byteLength < length)
            byteLength = length
        buffer.put(index, value)
    }

    override fun get(index: Int): Short = buffer[index]

}