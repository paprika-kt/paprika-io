package paprika.io.buffer

import java.nio.ByteBuffer
import java.nio.ByteOrder

actual class UByteArrayBuffer actual constructor(override val capacity: Int) :
    TypedArrayBuffer<UByte> {

    override val buffer: ByteBuffer = ByteBuffer.allocateDirect(capacity)
        .order(ByteOrder.nativeOrder())

    override var size: Int = this.capacity

    override var byteLength: Int = 0
    override var position: Int
        get() = buffer.position()
        set(value) {
            buffer.position(value)
        }

    override fun reset() {
        buffer.position(0)
        byteLength = 0
    }

    override fun set(index: Int, value: UByte) {
        if (byteLength < index)
            byteLength = index

        buffer.put(index, value.toByte())
    }

    override fun get(index: Int): UByte {
        if (index == -1) {
            buffer.flip()
            return 0.toUByte()
        }
        return buffer[index].toUByte()
    }

}

