package paprika.io.sources

import android.graphics.Bitmap
import paprika.canvas.sources.ImageSource
import paprika.io.buffer.UByteArrayBuffer

class AndroidImageSource(val bitmap: Bitmap) : ImageSource {
    override val width: Int = bitmap.width
    override val height: Int = bitmap.height
    override val bytes: UByteArrayBuffer by lazy {
        TODO()
    }
}