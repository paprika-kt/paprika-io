package paprika.io.buffer

import kotlinx.cinterop.*

actual class IntArrayBuffer actual constructor(override val size: Int) : TypedArrayBuffer<Int> {

    val source = nativeHeap.allocArray<IntVar>(size)
    override var byteLength: Int = 0
        private set
    override var position: Int = 0


    override fun set(index: Int, value: Int) {
        val pos = (index + 1) * 4
        if (pos > byteLength)
            byteLength = pos
        source[index] = value
    }

    override fun get(index: Int): Int = source[index]
    override val capacity: Int = size * 4

    override fun reset() {
        position = 0
        byteLength = 0
    }
}