package paprika.io.buffer

import kotlinx.cinterop.*

actual class ByteArrayBuffer actual constructor(override val capacity: Int) : TypedArrayBuffer<Byte> {

    //Review
    override val size: Int = this.capacity
    val source = nativeHeap.allocArray<ByteVar>(capacity)
    override var byteLength: Int = 0
        private set
    override var position: Int = 0


    override fun set(index: Int, value: Byte) {
        if (index > byteLength)
            byteLength = index + 1
        source[index] = value
    }

    override fun get(index: Int): Byte = source[index]

    override fun reset() {
        position = 0
        byteLength = 0
    }
}
