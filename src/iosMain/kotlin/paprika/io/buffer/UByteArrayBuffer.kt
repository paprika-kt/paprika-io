package paprika.io.buffer

import kotlinx.cinterop.*

actual class UByteArrayBuffer actual constructor(override val capacity: Int) :
    TypedArrayBuffer<UByte> {

    //Review
    override val size: Int = this.capacity
    val source = nativeHeap.allocArray<UByteVar>(size)
    override var byteLength: Int = 0
        private set
    override var position: Int = 0


    override fun set(index: Int, value: UByte) {
        if (index > byteLength)
            byteLength = index + 1
        source[index] = value
    }

    override fun get(index: Int): UByte = source[index]

    override fun reset() {
        position = 0
        byteLength = 0
    }
}

