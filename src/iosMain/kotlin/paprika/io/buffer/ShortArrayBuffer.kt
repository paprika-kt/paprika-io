package paprika.io.buffer

import kotlinx.cinterop.*

actual class ShortArrayBuffer actual constructor(override val size: Int) : TypedArrayBuffer<Short> {

    override val capacity: Int = size * 2
    val source = nativeHeap.allocArray<ShortVar>(size)

    override var byteLength: Int = 0
        private set


    override var position: Int = 0

    override fun set(index: Int, value: Short) {
        val pos = (index + 1) * 4
        if (pos > byteLength)
            byteLength = pos
        source[index] = value
    }

    override fun get(index: Int): Short = source[index]

    override fun reset() {
        position = 0
        byteLength = 0
    }
}