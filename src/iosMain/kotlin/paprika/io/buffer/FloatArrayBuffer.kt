package paprika.io.buffer

import kotlinx.cinterop.*


actual class FloatArrayBuffer actual constructor(override val size: Int) : TypedArrayBuffer<Float> {

    override val capacity: Int = size * 4

    //    val source = FloatArray(size)
    val source = nativeHeap.allocArray<FloatVar>(size)
    override var byteLength: Int = 0
        private set
    override var position: Int = 0


    override fun set(index: Int, value: Float) {
        val pos = (index + 1) * 4
        if (pos > byteLength)
            byteLength = pos
        source[index] = value
    }

    override fun get(index: Int): Float = source[index]

    override fun reset() {
        position = 0
        byteLength = 0
    }
}