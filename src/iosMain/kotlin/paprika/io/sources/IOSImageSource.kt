package paprika.io.sources

import paprika.canvas.sources.ImageSource
import paprika.io.buffer.UByteArrayBuffer
import platform.CoreGraphics.*
import platform.UIKit.UIImage


class IOSImageSource(
    override val width: Int,
    override val height: Int,
    override val bytes: UByteArrayBuffer,
    val image: CGImageRef? = null
) :
    ImageSource {

    //    constructor(image: UIImage) : this(CGImageGetWidth(image.CGImage).toInt(), CGImageGetHeight(image.CGImage).toInt(), UIImageToImageSource(image)!!)
    constructor(image: UIImage, data: Pair<CGImageRef, UByteArrayBuffer> = UIImageToImageSource4(image)) : this(
        CGImageGetWidth(image.CGImage).toInt(),
        CGImageGetHeight(image.CGImage).toInt(),
        data.second,
        data.first
    )

}


fun UIImageToImageSource4(image: UIImage): Pair<CGImageRef, UByteArrayBuffer> {
    val width = CGImageGetWidth(image.CGImage)
    val height = CGImageGetHeight(image.CGImage)
    val imageRef = image.CGImage ?: error("Failed to load image")

//    val textureData = UByteArray((width * height).toInt() * 4)
    val textureData = UByteArrayBuffer((width * height).toInt() * 4)
//    val textureData = nativeHeap.allocArray<UByteVar>((width * height).toInt() * 4)
    val colorSpace = CGColorSpaceCreateDeviceRGB()
    val bytesPerPixel = 4
    val bytesPerRow = bytesPerPixel * width.toInt()
    val bitsPerComponent = 8


    val context = CGBitmapContextCreate(
        textureData.source, width, height,
        bitsPerComponent.toULong(), bytesPerRow.toULong(), colorSpace,
        CGImageAlphaInfo.kCGImageAlphaPremultipliedLast.value or kCGBitmapByteOrder32Big
    )

    CGColorSpaceRelease(colorSpace)

    CGContextDrawImage(context, CGRectMake(0.0, 0.0, width.toDouble(), height.toDouble()), imageRef)
    CGContextRelease(context);


    return imageRef to textureData
}
