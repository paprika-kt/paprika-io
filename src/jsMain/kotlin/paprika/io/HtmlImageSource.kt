package paprika.io

import org.khronos.webgl.get
import org.w3c.dom.CanvasRenderingContext2D
import org.w3c.dom.HTMLCanvasElement
import org.w3c.dom.HTMLImageElement
import paprika.canvas.sources.ImageSource
import paprika.io.buffer.UByteArrayBuffer
import kotlin.browser.document

class HtmlImageSource(val image: HTMLImageElement) : ImageSource {
    override val width: Int = image.width
    override val height: Int = image.height
    override val bytes: UByteArrayBuffer by lazy {
        image.toUByte()
    }
}

fun HTMLImageElement.toUByte(): UByteArrayBuffer {
    val canvas = document.createElement("canvas") as HTMLCanvasElement
    val context = canvas.getContext("2d") as CanvasRenderingContext2D;
    canvas.width = this.width
    canvas.height = this.height
    context.drawImage(this, 0.0, 0.0)
    val data = context.getImageData(0.0, 0.0, canvas.width.toDouble(), canvas.height.toDouble()).data
    val ub = UByteArrayBuffer(data.length)
    repeat(data.length) {
        ub[it] = data[it].toUByte()
    }
    return ub
}