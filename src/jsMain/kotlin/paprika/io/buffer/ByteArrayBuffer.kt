package paprika.io.buffer

import org.khronos.webgl.Int8Array
import org.khronos.webgl.get
import org.khronos.webgl.set

actual class ByteArrayBuffer actual constructor(override val capacity: Int) : TypedArrayBuffer<Byte> {

    override val size: Int = this.capacity
    override val source = Int8Array(this.size)
    override var byteLength: Int = 0
        private set
    override var position: Int = 0


    override fun set(index: Int, value: Byte) {
        if (index > byteLength)
            byteLength = index
        source[index] = value
    }

    override fun get(index: Int): Byte = source[index]

    override fun reset() {
        position = 0
        byteLength = 0
    }
}

