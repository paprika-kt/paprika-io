package paprika.io.buffer

import org.khronos.webgl.*

actual class FloatArrayBuffer actual constructor(override val size: Int)
    : TypedArrayBuffer<Float> {

    override val capacity: Int
        get() = TODO("not implemented") //To change initializer of created properties use File | Settings | File Templates.


    override val source = Float32Array(size)
    override var byteLength: Int = 0
        private set
    override var position: Int = 0


    override fun set(index: Int, value: Float) {
        if (index > byteLength)
            byteLength = index
        source[index] = value
    }

    override fun get(index: Int): Float = source[index]

    override fun reset() {
        position = 0
        byteLength = 0
    }
}