package paprika.io.buffer

import org.khronos.webgl.ArrayBuffer
import org.khronos.webgl.ArrayBufferView

actual interface Buffer : ArrayBufferView {
    // var offsetPosition: Int review
    actual val capacity: Int
    actual override val byteLength: Int
    actual var position: Int
    actual fun reset()

    val source: ArrayBufferView
    //TODO review
    override val buffer: ArrayBuffer
        get() = source.buffer
    override val byteOffset: Int
        get() = 0

}