package paprika.io.buffer

import org.khronos.webgl.Int32Array
import org.khronos.webgl.get
import org.khronos.webgl.set

actual class IntArrayBuffer actual constructor(override val size: Int)
    : TypedArrayBuffer<Int> {

    override val source = Int32Array(size)
    override var byteLength: Int = 0
        private set
    override var position: Int = 0


    override fun set(index: Int, value: Int) {
        if (index > byteLength)
            byteLength = index
        source[index] = value
    }

    override fun get(index: Int): Int = source[index]
    override val capacity: Int
        get() = TODO("not implemented") //To change initializer of created properties use File | Settings | File Templates.

    override fun reset() {
        position = 0
        byteLength = 0
    }
}