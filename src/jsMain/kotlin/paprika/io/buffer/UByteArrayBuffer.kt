package paprika.io.buffer

import org.khronos.webgl.Int8Array
import org.khronos.webgl.Uint8Array
import org.khronos.webgl.get
import org.khronos.webgl.set

actual class UByteArrayBuffer actual constructor(override val capacity: Int) :
    TypedArrayBuffer<UByte> {

    override val size: Int = this.capacity
    override val source = Uint8Array(this.size)
    override var byteLength: Int = 0
        private set
    override var position: Int = 0


    override fun set(index: Int, value: UByte) {
        if (index > byteLength)
            byteLength = index
        this.source[index] = value.toByte()
    }

    override fun get(index: Int): UByte = source[index].toUByte()

    override fun reset() {
        position = 0
        byteLength = 0
    }
}

