package paprika.io.buffer

import org.khronos.webgl.*

actual class ShortArrayBuffer actual constructor(override val size: Int)
    : TypedArrayBuffer<Short> {

    override val source = Int16Array(size)
    override var byteLength: Int = 0
        private set
    override var position: Int = 0


    override fun set(index: Int, value: Short) {
        if (index > byteLength)
            byteLength = index
        source[index] = value
    }

    override fun get(index: Int): Short = source[index]
    override val capacity: Int
        get() = TODO("not implemented") //To change initializer of created properties use File | Settings | File Templates.

    override fun reset() {
        position = 0
        byteLength = 0
    }
}