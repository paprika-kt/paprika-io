package paprika.io.task

import paprika.io.shouldBe
import kotlin.test.*


class TaskTest {

    @Test
    fun `Should be sync`() {
        assertFails { Task.resolve(2).execute { fail("is async") } }
    }

    @Test
    fun `Should not start before call execute`() {
        Task.resolve(2).action { fail("is called") }
        Task<Int> { _, _ -> fail("is called") }
    }

    @Test
    fun `Should start on execute and pass value`() {
        Task.resolve(2).execute { it shouldBe 2 }
        Task<Int> { resolve, _ -> resolve(1) }.execute { it shouldBe 1 }
    }

    @Test
    fun `Should not throw exception before execute`() {
        Task.reject<Int>(UnsupportedOperationException())
    }

    @Test
    fun `Should throw exception on execute`() {
        assertFailsWith<UnsupportedOperationException> {
            Task.reject<Int>(UnsupportedOperationException()).execute { }
        }
    }

    @Test
    fun `should pass exception on execute if listen error`() {
        Task.reject<Int>(UnsupportedOperationException())
            .execute({
                assertTrue(it is UnsupportedOperationException, "error should be UnsupportedOperationException")
            }) { }
    }


    @Test
    fun `should be able to chain Task`() {
        Task.resolve(1).then { Task.resolve(1 + it) }.execute { it shouldBe 2 }
        Task.resolve(1).then { Task.resolve("a") }.execute { it shouldBe "a" }
    }

    @Test
    fun `should be able to add action in chaining Task`() {
        Task.resolve(1).action { it shouldBe 1 }.execute { it shouldBe 1 }
        Task.resolve(1).action { 1 + 1 }.execute { it shouldBe 1 }
    }

    @Test
    fun `should be able to add change value in chaining Task`() {
        Task.resolve(1).map { 1 + 1 }.execute { it shouldBe 2 }
    }

    @Test
    fun `should be able to execute a list of task`() {
        Task.all(Task.resolve(1), Task.resolve(2))
            .execute {
                val (v1, v2) = it
                v1 shouldBe 1
                v2 shouldBe 2
            }
        Task.all(Task.resolve(1), Task.resolve(2), Task.resolve(3))
            .execute {
                val (v1, v2, v3) = it
                v1 shouldBe 1
                v2 shouldBe 2
                v3 shouldBe 3
            }
        Task.all(Task.resolve(1), Task.resolve(2), Task.resolve(3), Task.resolve(4))
            .execute {
                val (v1, v2, v3, v4) = it
                v1 shouldBe 1
                v2 shouldBe 2
                v3 shouldBe 3
                v4 shouldBe 4
            }

        val tasks = listOf(Task.resolve(1), Task.resolve(2), Task.resolve(3), Task.resolve(4), Task.resolve(5))
        Task.all(tasks)
            .execute {
                assertTrue(it.containsAll(listOf(1, 2, 3, 4, 5)))
            }
        tasks.taskAll().execute {
            assertTrue(it.containsAll(listOf(1, 2, 3, 4, 5)))
        }
    }

    @Test
    fun `should be able to execute a list of task has sequence`() {
        var counter = 0
        Task.seq(listOf(1, 2, 3, 4)) {
            counter += it
            Task.resolve(it)
        }.execute {
            counter shouldBe 10
            it shouldBe 4
        }
        val tasks = listOf({ i: Int -> Task.resolve(i + 2) }, { i -> Task.resolve(i + 3) }, { i -> Task.resolve(i + 4) })
        Task.resolve(1).seq(tasks).execute { it shouldBe 10 }

    }
}