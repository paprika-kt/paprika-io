package paprika.io

import kotlin.test.Test


class PathTest {
    @Test
    fun `Path baseName`() {
        Path.baseName("file") shouldBe "file"
        Path.baseName("c:/file") shouldBe "file"
        Path.baseName("file.txt") shouldBe "file.txt"
        Path.baseName("a/b/c/file.txt") shouldBe "file.txt"
        Path.baseName("a/b/c/fi%?&*(le") shouldBe "fi%?&*(le"
    }

    @Test
    fun `Path dirName`() {
        Path.dirName("file") shouldBe ""
        Path.dirName("dir/") shouldBe "dir"
        Path.dirName("abc/file") shouldBe "abc"
        Path.dirName("ab%?&*(c/file") shouldBe "ab%?&*(c"
        Path.dirName("http://abc/file") shouldBe "http://abc"
    }

    @Test
    fun `Path extension`() {
        Path.extension("file") shouldBe null
        Path.extension("file.txt") shouldBe "txt"
        Path.extension("file.gz.tar") shouldBe "tar" //TODO create a function for get full extension
    }

    @Test
    fun `Path protocol`() {
        Path.protocol("http://file") shouldBe "http"
        Path.protocol("r1s://file") shouldBe "r1s"
    }

    @Test
    fun `Path join`() {
        Path.join("dir1", "file") shouldBe "dir1/file"
        Path.join("dir2", "./file") shouldBe "dir2/./file"
        Path.join("dir3", "../file") shouldBe "dir3/../file"
        Path.join("dir4/", "file") shouldBe "dir4/file"
        Path.join("dir5/", "/file") shouldBe "/file"
        Path.join("dir6/dir8", "dir7/file") shouldBe "dir6/dir8/dir7/file"
    }
}

