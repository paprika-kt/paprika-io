package paprika.io.buffer

import java.nio.Buffer

actual interface Buffer {
    // var offsetPosition: Int review
    actual val capacity: Int
    actual val byteLength: Int
    actual var position: Int
    actual fun reset()
    val buffer: Buffer

}