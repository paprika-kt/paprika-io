package paprika.io.buffer

import java.nio.ByteBuffer
import java.nio.ByteOrder

actual class FloatArrayBuffer actual constructor(override val size: Int)
    : TypedArrayBuffer<Float> {

    override var capacity: Int = size * 4
    override var byteLength: Int = 0
    override var position: Int
        get() = buffer.position()
        set(value) {
            buffer.position(value)
        }

    override fun reset() {
        buffer.position(0)
        byteLength = 0
    }

    override val buffer = ByteBuffer.allocateDirect(size * 4)
        .order(ByteOrder.nativeOrder())
        .asFloatBuffer()

    override fun set(index: Int, value: Float) {
        val length = (index + 1) * 4
        if (byteLength < length)
            byteLength = length
        buffer.put(index, value)
    }

    override fun get(index: Int): Float = buffer[index]

}