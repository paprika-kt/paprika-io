package paprika.io.buffer

import java.nio.ByteBuffer
import java.nio.ByteOrder

actual class IntArrayBuffer actual constructor(override val size: Int)
    : TypedArrayBuffer<Int> {

    override var capacity: Int = size * 4
    override var byteLength: Int = 0
        get() = buffer.limit()
    override var position: Int
        get() = buffer.position()
        set(value) {
            buffer.position(value)
        }

    override fun reset() {
        buffer.position(0)
        byteLength = 0
    }

    override val buffer = ByteBuffer.allocateDirect(size * 4)
        .order(ByteOrder.nativeOrder())
        .asIntBuffer()

    override fun set(index: Int, value: Int) {
        val length = (index + 1) * 4
        if (byteLength < length)
            byteLength = length
        buffer.put(index, value)
    }

    override fun get(index: Int): Int = buffer[index]

}

fun IntArrayBuffer.set(value: Int): java.nio.IntBuffer? {
    this[0] = value
    return buffer
}