package paprika.io.buffer

import java.nio.ByteBuffer
import java.nio.ByteOrder

actual class ByteArrayBuffer(override val buffer: ByteBuffer) : TypedArrayBuffer<Byte> {

    actual constructor(capacity: Int) : this(
        ByteBuffer.allocateDirect(capacity)
            .order(ByteOrder.nativeOrder())
    )

    override val capacity: Int = buffer.capacity()
    override var size: Int = this.capacity

    override var byteLength: Int = 0
    override var position: Int
        get() = buffer.position()
        set(value) {
            buffer.position(value)
        }

    fun set(byteArray: ByteArray) {
        byteArray.forEachIndexed { index, byte -> set(index, byte) }
    }

    override fun reset() {
        buffer.position(0)
        byteLength = 0
    }

    override fun set(index: Int, value: Byte) {
        if (byteLength < index)
            byteLength = index

        buffer.put(index, value)
    }

    override fun get(index: Int): Byte {
        if (index == -1) {
            buffer.flip()
            return 0.toByte()
        }
        return buffer[index]
    }

}

