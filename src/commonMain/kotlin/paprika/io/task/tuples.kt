@file:Suppress("UNCHECKED_CAST", "unused")

package paprika.io.task


//TODO review separate advance async into other lib (separate from couroutine)
data class Tuple2<T1, T2>(val value1: T1, val value2: T2)

data class Tuple3<T1, T2, T3>(val value1: T1, val value2: T2, val value3: T3)
data class Tuple4<T1, T2, T3, T4>(val value1: T1, val value2: T2, val value3: T3, val value4: T4)


fun <T1, T2> Task.TaskCompanion.all(task1: Task<T1>, task2: Task<T2>) =
    all(listOf(task1 as Task<Any>, task2 as Task<Any>))
        .map { Tuple2(it[0] as T1, it[1] as T2) }

fun <T1, T2, T3> Task.TaskCompanion.all(task1: Task<T1>, task2: Task<T2>, task3: Task<T3>) =
    all(listOf(task1 as Task<Any>, task2 as Task<Any>, task3 as Task<Any>))
        .map { Tuple3(it[0] as T1, it[1] as T2, it[2] as T3) }

fun <T1, T2, T3, T4> Task.TaskCompanion.all(task1: Task<T1>, task2: Task<T2>, task3: Task<T3>, task4: Task<T4>) =
    all(listOf(task1 as Task<Any>, task2 as Task<Any>, task3 as Task<Any>, task4 as Task<Any>))
        .map { Tuple4(it[0] as T1, it[1] as T2, it[2] as T3, it[3] as T4) }
