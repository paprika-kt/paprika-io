@file:Suppress("UNCHECKED_CAST", "unused")

package paprika.io.task

import ca.jsbr.disposable.CompositeDisposable
import ca.jsbr.disposable.Disposable


typealias Completable = Task<Unit>

/**
 * Task is similar to Promise or rx.Single and represents the eventual completion (or failure) operation started
 * when `execute` is called.
 *
 * This can be compared to a cold (not executed immediately) Promise
 *
 * Task is not really async
 *
 *
 *
 * @param operation action to execute
 */
class Task<T : Any?>(private val operation: (resolve: (T) -> Unit, reject: (Throwable) -> Unit) -> Unit) : Disposable {

    private var sucessed: Boolean = false
    private var executed: Boolean = false
    private var value: T? = null
    private var error: Throwable? = null

    private val successCallback = arrayListOf<(T) -> Unit>()
    private val failCallback = arrayListOf<(Throwable) -> Unit>()

    init {

    }

    companion object TaskCompanion {
        fun resolve(): Completable = resolve(Unit)
        fun complete(): Completable = resolve(Unit)
        fun <T> resolve(value: T): Task<T> = Task { resolve, _ -> resolve(value) }
        fun <T> reject(value: Throwable): Task<T> = Task { _, reject -> reject(value) }

        fun <T> all(tasks: List<Task<T>>): Task<List<T>> = Task { resolve, reject ->
            var count = tasks.size
            val result = Array<Any?>(tasks.size) { null }
            tasks.forEachIndexed { index, it ->
                it.execute({
                    reject(it)
                }, {
                    count--
                    result[index] = it
                    if (count <= 0)
                        resolve(result.map { it as T })
                })
            }
        }

    }


    inline fun <reified E : Throwable> catch(crossinline action: (Throwable) -> Task<T>): Task<T> =
        Task { resolve, reject ->
            this.execute({
                if (it is E) action(it).execute(reject, resolve)
                else reject(it)
            }, resolve)
        }

    fun <R> then(next: (T) -> Task<R>): Task<R> = Task { resolve, reject ->
        this.execute(reject, {
            try {
                next(it).execute(reject, resolve)
            } catch (e: Throwable) {
                reject(e)
            }
        })
    }

    override fun dispose() {
        sucessed = false
        this.value = null
        error = null
        successCallback.clear()
        failCallback.clear()
    }

    private fun internalSuccess(value: T) {
        sucessed = true
        this.value = value
        successCallback.forEach { it(value) }
        successCallback.clear()
        failCallback.clear()
    }

    fun execute(fail: ((e: Throwable) -> Unit)? = null, success: ((T) -> Unit)? = null): Disposable {
        if (error != null) {
            fail?.invoke(error!!)
            return Disposable.empty
        }
        if (sucessed) {
            success?.invoke(value as T)
            return Disposable.empty
        }

        val disposable = CompositeDisposable()
        if (fail != null) {
            failCallback.add(fail)
            disposable.add { failCallback.remove(fail) }
        }
        if (success != null) {
            successCallback.add(success)
            disposable.add { successCallback.remove(success) }
        }

        try {

            if (!executed) {
                executed = true
                operation(this::internalSuccess, this::internalFail)
            }
            return disposable
        } catch (e: Throwable) {
            this.internalFail(e)
        }
        return Disposable.empty
    }

    private fun internalFail(err: Throwable) {
        error = err
        if (failCallback.size == 0) {
            successCallback.clear()
            failCallback.clear()
            throw err
        } else
            if (failCallback.size != 0) {
                failCallback.forEach { it(err) }
                successCallback.clear()
                failCallback.clear()
            }
    }
}

fun <T> task(trigger: (TaskTrigger<T>) -> Unit) = Task<T> { resolve, reject -> trigger(TaskTrigger(resolve, reject)) }

class TaskTrigger<T>(val resolve: ((T) -> Unit), val reject: ((Throwable) -> Unit))

fun TaskTrigger<Unit>.resolve() = resolve(Unit)

fun <T, R> Task.TaskCompanion.seq(iterable: Iterable<T>, initial: R, action: (T, R) -> Task<R>): Task<R> {
    val iterator = iterable.iterator()
    if (!iterator.hasNext()) return reject(NoSuchElementException("Task.Sequence iterable is empty"))
    var promise: Task<R> = action(iterator.next(), initial)
    while (iterator.hasNext()) {
        val next = iterator.next()
        promise = promise.then { action(next, it) }
    }
    return promise
}

//Review
fun <T, R> Task.TaskCompanion.seq(iterable: Iterable<T>, action: (T) -> Task<R>): Task<R> {
    val iterator = iterable.iterator()
    if (!iterator.hasNext()) return reject(NoSuchElementException("Task.Sequence iterable is empty"))
    var promise: Task<R> = action(iterator.next())
    while (iterator.hasNext()) {
        val next = iterator.next()
        promise = promise.then { action(next) }
    }
    return promise
}

fun <T> List<Task<T>>.taskAll() =
    Task.all(this)

fun <T> List<Task<T>>.taskSeq() =
    this.fold(Task.reject<T>(Throwable())) { acc, task ->
        acc.catch<Throwable> { task }.then { task }
    }

fun <T, R> Task<R>.seq(iterable: Iterable<T>, action: (T, R?) -> Task<R>) =
    this.then { Task.seq(iterable, it, action) }

fun <T> Task<T>.seq(actions: Iterable<(T) -> Task<T>>): Task<T> {
    return actions.fold(this) { acc, action ->
        acc.then { action(it) }
    }
}

fun <T, R> Task<T>.map(action: (T) -> R) = then { Task.resolve(action(it)) }
fun <T> Task<T>.action(action: (T) -> Unit) = then { action(it); Task.resolve(it) }
fun <T> Task<T>.toUnit(): Completable = then { Task.resolve() }
fun <T> Task<T>.throwError() = execute(fail = { throw it })
