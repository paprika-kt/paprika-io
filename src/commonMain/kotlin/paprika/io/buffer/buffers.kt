package paprika.io.buffer

expect class ByteArrayBuffer(capacity: Int)
    : TypedArrayBuffer<Byte>


expect class UByteArrayBuffer(capacity: Int)
    : TypedArrayBuffer<UByte>

expect class FloatArrayBuffer(size: Int)
    : TypedArrayBuffer<Float>

expect class IntArrayBuffer(size: Int)
    : TypedArrayBuffer<Int>

expect class ShortArrayBuffer(size: Int)
    : TypedArrayBuffer<Short>


operator fun ByteArrayBuffer.set(index: Int, value: UShort) {
    set(index, value.toByte())
}
