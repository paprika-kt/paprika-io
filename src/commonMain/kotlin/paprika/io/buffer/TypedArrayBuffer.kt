package paprika.io.buffer

interface TypedArrayBuffer<T> : Buffer {

    // Count of T in the buffer
    val size: Int

    operator fun set(index: Int, value: T)
    fun set(collection: Collection<T>) = collection.forEachIndexed() { i, t -> set(i, t) }
    operator fun get(index: Int): T
}