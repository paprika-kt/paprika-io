package paprika.io.buffer

expect interface Buffer {
    /**
     * Max byte allowed
     */
    val capacity: Int
    /**
     * contiguous byteElement (approximation)
     */
    val byteLength: Int
    var position: Int
    // var offsetPosition: Int review

    fun reset()
}

