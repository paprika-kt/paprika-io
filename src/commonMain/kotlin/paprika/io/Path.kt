package paprika.io

//Review inline class?
class Path(val path: String) {

    companion object {
        private var regExExtension = "^.+\\.([^.]+)$".toRegex()
        private var regExRemoveExtension = Regex("(.+?)(.[^.]+$|$)")
        private var regExFile = Regex("^(.+)/([^/]+)?$")

        fun extension(path: String) = regExExtension.find(path)?.groupValues?.get(1)
        fun removeExtension(path: String) = regExRemoveExtension.find(path)?.groupValues?.get(1) ?: path
        fun baseName(path: String) = regExFile.find(path)?.groupValues?.get(2) ?: path
        fun dirName(path: String) = regExFile.find(path)?.groupValues?.get(1) ?: ""
        fun split(path: String) = path.split("/")
        fun removeFirstDash(path: String) = if (path.startsWith("/")) path.substring(1) else path
        fun removeProtocol(path: String): String {
            val index = path.indexOf("://")
            if (index == -1) return path
            return path.substring(path.indexOf("://") + 3)
        }

        fun protocol(path: String) = path.substring(0, path.indexOf("://"))

        fun join(base: String, path: String): String {
            removeFirstDash(path)
            if (path.startsWith("/")) return path
            if (base.endsWith("/"))
                return base + path
            return "$base/$path"
        }
    }

    val extension by lazy { extension(path) }
    val baseName by lazy { baseName(path) }
    val protocol by lazy { protocol(path) }
}