package paprika.canvas.sources

import paprika.io.buffer.ByteArrayBuffer

interface Source {
    val path: String
    val buffer: ByteArrayBuffer
}