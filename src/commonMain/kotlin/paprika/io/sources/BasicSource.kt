package paprika.io.sources

import paprika.canvas.sources.Source
import paprika.io.buffer.ByteArrayBuffer

class BasicSource(override val path: String, override val buffer: ByteArrayBuffer) : Source {
}