package paprika.canvas.sources

import paprika.io.buffer.UByteArrayBuffer

interface ImageSource {
    val width: Int
    val height: Int
    val bytes: UByteArrayBuffer
}